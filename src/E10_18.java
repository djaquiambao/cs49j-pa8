////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/7/2020
//  Description: Features event listeners that listens in on the buttons and counts how many times they are pressed.
///////////////////////////////////////
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class E10_18{

    public static void main(String[] args) {

        JFrame frame = new JFrame();


        //Instantiate JButton object and set its position on the frame
        JButton clickCounter_1 = new JButton("Click Me!");
        clickCounter_1.setLayout(null);
        clickCounter_1.setSize(100, 60);
        JLabel label1 = new JLabel("Click count: ");
        JPanel panel1 = new JPanel();
        panel1.add(label1);
        panel1.add(clickCounter_1);
        frame.add(panel1, BorderLayout.NORTH);

        //Listener for first button
        class ButtonClicker_1 implements ActionListener{
            public int clicked = 0;

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("(Button 1): I was clicked " + ++clicked + " times");
                label1.setText("Click count: " + clicked);
            }

        }

        //Instantiate listener to first button
        ActionListener click1 = new ButtonClicker_1();
        clickCounter_1.addActionListener(click1);

        //Instantiate and set the position of the second button on the frame
        JButton clickCounter_2 = new JButton("Click ME!");
        clickCounter_2.setSize(100, 60);
        JLabel label2 = new JLabel("Click count: ");
        JPanel panel2 = new JPanel();
        panel2.add(label2);
        panel2.add(clickCounter_2);
        frame.add(panel2, BorderLayout.CENTER);

        //Implement ActionListener interface for second button that listens in on the second button and keeps track of how much is counted.
        class ButtonClicker_2 implements ActionListener{
            public int clicked = 0;
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("(Button 2): I was clicked " + ++clicked + " times");
                label2.setText("Click count: " +clicked);
            }
        }

        //Instantiate listener to second button
        ActionListener click2 = new ButtonClicker_2();
        clickCounter_2.addActionListener(click2);

        frame.setSize(300, 400);
        frame.setTitle("E10_18");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);

    }

}
