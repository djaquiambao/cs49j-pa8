////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/9/2020
//  Description: This program demonstrates basic usage of the Timer API, Java Swing APIs, and Draw functions
//  which animates a constructed Car that moves diagonally across the Frame at 1s intervals for 10 units in the x and y
//  direction.
///////////////////////////////////////
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import javax.swing.Timer;

class Car
{
    public int xLeft;
    public int yTop;
    public Car(int x, int y)
    {
        xLeft = x;
        yTop = y;
    }
    public void draw(Graphics2D g2)
    {
        Rectangle body = new Rectangle(xLeft, yTop + 10, 60, 10);
        Ellipse2D.Double frontTire = new Ellipse2D.Double(xLeft +10, yTop + 20, 10, 10);
        Ellipse2D.Double rearTire = new Ellipse2D.Double(xLeft + 40, yTop + 20, 10, 10);

        Point2D.Double r1 = new Point2D.Double(xLeft + 10, yTop + 10);
        Point2D.Double r2 = new Point2D.Double(xLeft + 20, yTop);
        Point2D.Double r3 = new Point2D.Double(xLeft + 40, yTop);
        Point2D.Double r4 = new Point2D.Double(xLeft + 50, yTop + 10);

        //
        Line2D.Double frontWindshield = new Line2D.Double(r1, r2);
        Line2D.Double roofTop = new Line2D.Double(r2, r3);
        Line2D.Double rearWindshield = new Line2D.Double(r3, r4);

        g2.draw(body);
        g2.draw(frontTire);
        g2.draw(rearTire);
        g2.draw(frontWindshield);
        g2.draw(roofTop);
        g2.draw(rearWindshield);
    }

}

class CarComponent extends JComponent
{
    private Car car1;
    public CarComponent()
    {
        car1 = new Car(0, 0);
    }
    public void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        car1.draw(g2);
    }
    public void moveCarBy(int x, int y)
    {
        if(car1.xLeft < getWidth() && car1.yTop < getHeight()) {
            car1.xLeft += x;
            car1.yTop += y;
            repaint();
        }
    }
}
class CarFrame extends JFrame
{
    final int HEIGHT = 400;
    final int WIDTH = 300;
    private CarComponent carComponent;
    class CarListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            carComponent.moveCarBy(10, 10);
        }
    }
    public CarFrame()
    {
        carComponent = new CarComponent();
        add(carComponent);
        setSize(WIDTH, HEIGHT);
        ActionListener carAnimation = new CarListener();
        Timer t = new Timer(1000, carAnimation);
        t.start();
    }

}

public class P10_22 {
    public static void main(String[] args) {
        JFrame frame = new CarFrame();
        frame.setTitle("Car Animation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
