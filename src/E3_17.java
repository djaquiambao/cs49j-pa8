////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/7/2020
//  Description: Draws a red circle or ELLIPSE and adjusts the size and shape of it depending on what the user resizes
//  the frame to.
///////////////////////////////////////
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import javax.swing.*;

class Ellipse extends JComponent
{
    public void paintComponent (Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;

        Ellipse2D.Double ellipse = new Ellipse2D.Double(getX(),getY(),getWidth(),getHeight());
        g2.draw(ellipse);
        g2.setColor(Color.RED);
        g2.fill(ellipse);
    }
}

public class E3_17 {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(400, 200);
        frame.setTitle("Colored Ellipse");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Ellipse ellipseComp = new Ellipse();
        frame.add(ellipseComp);
        frame.setVisible(true);
    }
}
